<?php

namespace Ave\Sms;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

final class Sms
{

    const SMS_URL = 'SMS_URL';
    const SMS_USER = 'SMS_USER';
    const SMS_PASSWORD = 'SMS_PASSWORD';
    const MAXIMUM_LENGTH = 100000000;

    private string $phone;
    private string $message;


    public function __construct(
        private string|null $url = null,
        private string|null $user = null,
        private string|null $password = null,
    )
    {
        $this->url = $url ?: $_ENV[self::SMS_URL] ?? null;
        $this->user = $user ?: $_ENV[self::SMS_USER] ?? null;
        $this->password = $password ?: $_ENV[self::SMS_PASSWORD] ?? null;
    }

    /**
     * Set to phone number
     * @param string $phone
     * @return $this
     */
    public function to(string $phone): self
    {
        $this->setPhone($phone);
        return $this;
    }

    /**
     * Build message text body
     * @param string $message
     * @return $this
     */
    public function body(string $message): self
    {
        $this->setMessage($message);
        return $this;
    }

    /**
     * Send message
     * @return ResponseInterface
     * @throws GuzzleException
     */

    public function send(): ResponseInterface
    {
         return (new Client())->request(
            method: 'POST',
            uri: $this->getUrl(),
            options: [
                'form_params' => [
                    'login' => $this->getUser(),
                    'clave' => $this->getPassword(),
                    'json' => json_encode(array([
                        'destino' => $this->getTo(),
                        'mensaje' => $this->getBody(),
                    ]))

                ],
            ]

        );
    }


    /**
     * Set Phone value
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        if ($phone == '') {
            throw new \InvalidArgumentException(sprintf('"%s" needs a phone number, it cannot be empty.', __CLASS__));
        }
        $this->phone = $phone;
    }


    /**
     * Set Text Message Value
     * @param string $message
     */
    private function setMessage(string $message): void
    {
        $this->assertTextIsValid($message);
        $this->message = $message;
    }


    /**
     * Validate  if the length of characters is valid
     * @param $text
     * @return void
     */
    private function assertTextIsValid($text): void
    {
        if (strlen($text) > self::MAXIMUM_LENGTH) {
            throw new \LengthException('The maximun length for a message text is ' . self::MAXIMUM_LENGTH . ' characters');
        }
    }


    /**
     * @return string|null
     */
    private function getUrl(): string|null
    {
        return $this->url;
    }

    /**
     * @return string
     */
    private function getBody(): string
    {
        return $this->message;
    }


    private function getTo(): string
    {
        return $this->phone;
    }


    /**
     * @return string|null
     */
    private function getUser(): ?string
    {
        return $this->user;
    }


    /**
     * @return string|null
     */
    private function getPassword(): ?string
    {
        return $this->password;
    }
}