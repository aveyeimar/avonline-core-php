<?php

namespace Ave\Mail;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mime\Email;


class Mail
{
    public function __invoke(): Email
    {
        return ( new Email())
            ->from($_ENV['EMAIL_FROM']);
    }
}