<?php
namespace Ave;

use Ave\Sms\Sms;

class Ave{


    /**
     * Return Sms instance
     * @return Sms
     */
    public function sms(): Sms
    {
        return new Sms();
    }


}


